Ext.define('lpad.util.Config', {
	requires: [
		// 'Ext.JSON',
		'Ext.Ajax'
	],
	statics: {
		_instance: null,
		getInstance: function () {
			if (this._instance === null) {
				this._instance = Ext.create('lpad.util.Config');
			}
			return this._instance;
		},
		get: function () {
			return this.getInstance().get.apply(this._instance, arguments);
		},
		set: function () {
			return this.getInstance().set.apply(this._instance, arguments);
		}
	},
	_data: {},
	get: function (key) {
		return this._data[key];
	},
	set: function (key, value) {
		if (typeof key === "string") {
			this._data[key] = value;
		} else if (typeof key === "object") {
			for (var i in key) {
				if (key.hasOwnProperty(i)) {
					this.set(i, key[i]);
				}
			}
		}
		return this;
	},
	_parse: function () {
		if (this.get('env') && this.get('environments')) {
			var envConfig = this.get('environments')[this.get('env')];
			if (!envConfig) {
				throw new Error(this.get('env') + " environment config not found!");
			}
			Ext.Object.each(envConfig, function (key, value) {
				this.set(key, value);
			}, this);
		}
	},
	loadFromFile: function (url, after) {
		Ext.Ajax.request({
			url: url,
			method: 'GET',
			disableCaching: true,

			success: function(response) {
				if (response.responseText) {
					this.set(Ext.JSON.decode(response.responseText));
					this._parse();
				}
				if (after) {
					after(response);
				}
			}.bind(this),
			failure: function (response) {
				if (after) {
					after(new Error("Config load error"));
				}
			}
		});
		return this;
	}
});
