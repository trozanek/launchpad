Ext.define('lpad.util.EsbRequest', {
	requires: [
		'Ext.util.JSONP',
		'lpad.util.Config'
	],
	statics: {
		makeLogRequest: function (logs, callback) {
			var json = Ext.JSON.encode(logs);

			Ext.data.JsonP.request({
				url: lpad.util.Config.get('esbHostUrl'),
				callbackKey: 'callback',
				params: {
					data: json
				},
				success: function(result, request) {
					if (callback) {
						callback(result);
					}
				},
				failure: function () {
					if (callback) {
						callback(new Error("Connection error"));
					}
				}
			});
		},
		/**
		* Sends device info to the server, and gets more advanced information
		*/
		makeDeviceInfoRequest: function (data, callback) {
			console.log(lpad.util.Config.get('useStaticData'));
			if (lpad.util.Config.get('useStaticData') !== true) {
				var json = Ext.JSON.encode(data);
				//console.log("URL:"+lpad.util.Config.get('esbHostUrl'));
				//Ext.data.JsonP.request({
				Ext.Ajax.request({
					url: lpad.util.Config.get('esbHostUrl'),
					callbackKey: 'callback',
					method:'get',
					params: {
						data: json
					},
					success: function(result, request) {
						callback(result);
					},
					failure: function () {
						callback(new Error("Connection error"));
					}
				});
			} else {
				Ext.Ajax.request({
					url: lpad.util.Config.get('deviceInfoStatic'),
					method: 'get',
					success: function (result, request) {
						callback(Ext.JSON.decode(result.responseText).resultSet);
					},
					failure: function () {
						callback(new Error("Connection error"));
					}
				});
			}

		},
		sendEmail: function (data, callback) {
			Ext.Ajax.request({
				url: 'http://mobile-poc.gene.com/esb/invoke/GneENT_Dispatcher.Services/invokeDispatcher?NSName=GneENT_Mobility.iPhone:sendEmail',
					params: {
					cType: 'json',
					apiKey: '5e78cc7c32f51b00137b1a606f1',
					toEmailAddress: data.sendTo,
					fromEmailAddress: data.replyTo,
					emailSubject: data.subject,
					emailBody: data.body
				},
				callbackKey: 'callback',
				success: callback,
				failure: function (r, o) {
					callback(new Error("Message was not sent."));
				},
				scope: this
			});
		},
		getEsbData: function (data) {
			var url;
			if (lpad.util.Config.get('useStaticData') === true) {
				if (data.staticUrl) {
					data.url = data.staticUrl;
				}
				Ext.Ajax.request(data);
			} else {
				if (!data.url) {
					throw new Error("No url defined for ESB request!");
				}
				//we can put here some more complicated logic, when needed. Can we attach apiKey here?
				if (!data.params) {
					data.params = {};
				}
				data.params.apiKey = lpad.util.Config.get('apiKey');
				Ext.Ajax.request(data);
			}
		}
	}
});
