Ext.define('lpad.util.springboard.DataView', {
    extend: 'Ext.DataView',

    requires: [
        'lpad.util.springboard.Item'
    ],

    config: {
        baseCls: 'my-dataview',
        useComponents: true,
        defaultType: 'springboarditem'
    }
});
