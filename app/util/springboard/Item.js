Ext.define('lpad.util.springboard.Item', {
    extend: 'Ext.dataview.component.DataItem',
    requires: [
    ],
    xtype: 'springboarditem',

    config: {
        dataMap: {
            getImage: {
                setIcon: 'icon'
            },
            getName: {
                setName: 'name'
            },
            getBadging: {
                setNumber: 'number'
            }
        },
        html: '<div><img src=""/><span class="name"></span><span class="badging"></span></div>',
        cls: 'my-dataview-item'
    },

    getImage: function () {
        if (!this.imageEl) {
            this.imageEl = {
                el: this.element.down('img'),
                setIcon: function (icon) {
                    this.el.set({
                        src: icon
                    });
                }
            };
        }
        return this.imageEl;
    },
    getName: function () {
        if (!this.nameEl) {
            this.nameEl = {
                el: this.element.down('span.name'),
                setName: function (name) {
                    this.el.setHtml(name);
                }
            };
        }
        return this.nameEl;
    },
    getBadging: function () {
        if (!this.badgingEl) {
            this.badgingEl = {
                el: this.element.down('span.badging'),
                setNumber: function (number) {
                    if (number | 0 > 0) {
                        this.el.setHtml(number);
                    } else {
                        this.el.setVisible(false);
                    }
                }
            };
        }
        return this.badgingEl;
    }
});
