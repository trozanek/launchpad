Ext.define('lpad.util.Session', {
	constructor: function(config) {
        this.initConfig(config);
    },
	hasSmCookie: function () {
		var session = false;
		var cookies = document.cookie.split(';');
	    for (var i=0;i<cookies.length;i++) {
	    	var item = cookies[i];
	    	if (item.indexOf('SMSESSION') > -1) {
	    		session = true;
	    	}
	    }
	    return session;
	}
});
