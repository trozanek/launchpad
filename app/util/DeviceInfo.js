Ext.define('lpad.util.DeviceInfo', {
	requires: [
		'Ext.os',
		'Ext.browser',
		'lpad.util.Config',
		'lpad.util.EsbRequest'
	],
	_data: {},

	constructor: function () {
		this.set(this._getDeviceData());
	},
	get: function (key) {
		return this._data[key];
	},
	set: function (key, value) {
		if (typeof key === "string") {
			this._data[key] = value;
		} else if (typeof key === "object") {
			for (var i in key) {
				if (key.hasOwnProperty(i)) {
					this.set(i, key[i]);
				}
			}
		}
		return this;
	},
	_getDeviceData: function () {
		return {
			os: {
				name: Ext.os.name,
				version: Ext.os.version.version,
				iPad: Ext.os.is.iPad || false,
				iPhone: Ext.os.is.iPhone || false
			},
			browser: {
				name: Ext.browser.name,
				engine: Ext.browser.engineName,
				version: Ext.browser.engineVersion.version
			}
		};
	},
	/**
	* Sends request to ESB service and loads returned data
	*
	*/
	askServer: function (callback) {
		if (!this.get('userLogin')) {
			console.dir(this);
			throw new Error("User login has to be provided, before sending device data to ESB");
		}
		lpad.util.EsbRequest.makeDeviceInfoRequest(this._data, function (data) {
			if (data instanceof Error) {
				callback(data);
				return;
			}
			callback(data);
		}.bind(this));
		return this;
	}
});
