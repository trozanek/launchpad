/**
* This class allows you to create synchronised instances of the Store. All created instances of this class,
* will continuously synchronize their data, so all stores will have the same collection of models.

* This type of Store can be used like a normal Store class, if you pass "syncData: false" as an option to the constructor,
* store will not synchronize its data and will behave just like normal Sencha store.
*
* You can also use stopSyncing(0) and startSyncing() methods to controll the store behaviour.
*/
Ext.define('lpad.util.data.SyncStore', {
    extend: 'Ext.data.Store',

    config: {
        syncData: true
    },
    statics: {
        _stores: {},

        _isSyncing: function (store) {
            return store.isSyncing === true;
        },

        _destroy: function (store) {
            var index = store._otherStores.indexOf(store);
            if (index !== -1) {
                store._otherStores.splice(index, 1);
            }
        },

        _onSelfRecordsAdd: function (basestore, records, options) {
            if (!this._isSyncing(basestore) || (options && options.firedByOtherStore === true)) {
                return;
            }
            console.log("on add");
            basestore._otherStores.filter(this._isSyncing).forEach(function (store) {
                var toAdd = [];
                if (store === basestore) {
                    return;
                }
                records.forEach(function (record) {
                    if (store.getData().all.indexOf(record) === -1) {
                        toAdd.push(record);
                    }
                });
                if (toAdd.length === 0) {
                    return;
                }
                store.suspendEvents();
                store.add(toAdd);
                store.resumeEvents();

                store.fireEvent('addrecords', store, toAdd, {
                    firedByOtherStore: true
                });
            });
        },
        _onSelfRecordsRemove: function (basestore, records, indices, options) {
            var toRemove = [];
            if (!this._isSyncing(basestore) || (options && options.firedByOtherStore === true)) {
                return;
            }
            records.forEach(function (record, index) {
                if (basestore.getData().all.indexOf(record) === -1) {
                    toRemove.push(record);
                }
            });
            console.log('on remove ');
            if (toRemove.length === 0) {
                return;
            }
            basestore._otherStores.filter(this._isSyncing).forEach(function (store) {
                if (store === basestore) {
                    return;
                }
                store.suspendEvents();
                store.remove(toRemove);
                store.resumeEvents();

                store.fireEvent('removerecords', store, toRemove, indices, {
                    firedByOtherStore: true
                });
            });
        },
        _onSelfLoad: function (basestore, records, successful, operation, options) {
            if (successful && (!this._isSyncing(basestore) || (options && options.firedByOtherStore === true))) {
                return;
            }
            console.log("On load");
            basestore._otherStores.filter(this._isSyncing).forEach(function (store) {
                if (store === basestore) {
                    return;
                }
                store._setDataFromStore(basestore);

                store.fireEvent('load', store, records, successful, operation, {
                    firedByOtherStore: true
                });
            });
        }
    },

    constructor: function () {
        var stat = lpad.util.data.SyncStore, _stores;
        this.callParent(arguments);

        this.isSyncing = false;
        if (this.getSyncData() === true) {
            this.isSyncing = true;

            //This listeners will be used to synchronize data between created stores
            this.addAfterListener('addrecords', stat._onSelfRecordsAdd, stat);
            this.addAfterListener('removerecords', stat._onSelfRecordsRemove, stat);
            this.addAfterListener('load', stat._onSelfLoad, stat);

            if(!Ext.isArray(stat._stores[this.$className])) {
                stat._stores[this.$className] = [];
            }
            _stores = stat._stores[this.$className];

            if (_stores.length !== 0) {
                this._setDataFromStore(_stores[0]);
            }
            _stores.push(this);
            this._otherStores = _stores;
        }
    },

    _setDataFromStore: function(basestore) {
        this.setData(basestore.getData().all.slice());
    },

    stopSyncing: function () {
        this.isSyncing = false;
    },
    startSyncing: function () {
        this.isSyncing = true;
    },
    loadIfNotLoaded: function () {
        if (this._otherStores.length === 0 || this._otherStores[0].getData().all.length === 0) {
            this.load.apply(this, arguments);
        }
    },
    /**
    * Removes the store from the index
    */
    destroy: function () {
        lpad.util.data.SyncStore._destroy(this);
    }
});
