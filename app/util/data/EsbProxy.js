Ext.define('lpad.util.data.EsbProxy', {
    requires: [
        'lpad.util.EsbRequest'
    ],
    extend: 'Ext.data.proxy.Ajax',
    alias: 'proxy.esb',

    doRequest: function (operation, callback, scope) {
        var writer  = this.getWriter(),
            request = this.buildRequest(operation);

        request.setConfig({
            headers        : this.getHeaders(),
            timeout        : this.getTimeout(),
            method         : this.getMethod(request),
            callback       : this.createRequestCallback(request, operation, callback, scope),
            url            : lpad.util.Config.get('useStaticData') === true ? this.config.staticUrl : this.url,
            scope          : this
        });

        if (operation.getWithCredentials() || this.getWithCredentials()) {
            request.setWithCredentials(true);
        }

        // We now always have the writer prepare the request
        request = writer.write(request);
        console.log("ESB PROXY", request.getCurrentConfig());
        lpad.util.EsbRequest.getEsbData(request.getCurrentConfig());

        return request;
    }
});
