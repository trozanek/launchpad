Ext.define('lpad.store.Apps', {
    extend: 'lpad.util.data.SyncStore',
    xtype: 'appsstore',
    config: {
        model: 'lpad.model.Apps'
    },
    compareExistingApps: function (appsList) {
        var toRemove = [], appsIds = [], store = this;

        appsList.forEach(function (app) {
            var index, record = {};
            //change fields names to lowercase
            for (var key in app) {
                if (app.hasOwnProperty(key)) {
                    record[key.toLowerCase()] = app[key];
                }
            }
            index = store.find('id', record.id);
            if (index === -1) {
                console.log('add', store.add(record));
            } else {
                console.log('update', record);
                store.getAt(index).set(record);
            }
            appsIds.push(record.id);
        });
        //we have to browse our models and remove those that were not included in applications list
        store.each(function (record) {
            if (appsIds.indexOf(record.get('id')) === -1) {
                console.log('remove', record);
                toRemove.push(record);
            }
        });
        store.remove(toRemove);
    }
});
