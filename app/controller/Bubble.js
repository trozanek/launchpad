Ext.define('lpad.controller.Bubble', {
    extend: 'Ext.app.Controller',
    requires: [
        'Ext.Anim'
    ],
    config: {
        control: {
			'bubble button': {
				tap: 'closeBubble'
			},
            'homepanel': {
                activate: 'showBookmarkBubble'
            }
        },
        localKey: 'lpad-hide-bubble'
    },

    launch: function() {

    },
    closeBubble: function(button) {
        button.up('bubble').destroy();
    },
    showBookmarkBubble: function (homepanel) {

        var lsKeyName = lpad.app.name + '-hide-bubble',
        // replace wacl.app.name with application's namespace
        displayCt = localStorage.getItem(lsKeyName) === null ? 0 : localStorage.getItem(lsKeyName);
        if (Ext.os.is.iPhone || Ext.os.is.iPad || Ext.os.is['iPhone Simulator'] || Ext.os.is['iPad Simulator']) {
            if (displayCt < 3) {
                Ext.Viewport.add(Ext.create('lpad.view.Bubble')).show({type:'slide',direction:'up',duration:2000});
                window.localStorage.setItem(lsKeyName, parseInt(displayCt, 10) + 1);
            }
        }
    }
});
