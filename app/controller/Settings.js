Ext.define('lpad.controller.Settings', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {

        },
        control: {
            'settingsview togglefield': {
                change: 'onFieldChange'
            },
            'homepanel toolbar button[action=back]': {
                tap: "goToHome"
            }
        }
    },
    onFieldChange: function (field, slider, thumb, newValue, oldValue) {
        var store = field.up('settingsview').getStore(),
            index = store.find('name', field.getName());

        if (index === -1) {
            throw new Error("Application " + field.getName() + " not found");
        }

        store.getAt(index).set('active', newValue);
        store.sync();
    },
    goToHome: function (button) {
        button.up('homepanel').setActiveItem(0);
    }
});
