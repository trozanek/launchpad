	Ext.define('lpad.controller.Main', {
    extend: 'Ext.app.Controller',
    xtype: 'maincontroller',

    requires: [
        'lpad.util.Config',
        'lpad.util.DeviceInfo',
        'lpad.util.Session',
        'lpad.view.Bubble'
    ],
    config: {
        refs: {
            'splash': 'splashpanel'
        },
        control: {
            'splashpanel': {
                activate: 'loadDeviceData'
            }
        },
        listeners: {
            'deviceinfoload': function (options) {
                if (options.isManaged) {
                    this.loadAppsList(options.splash);
                } else {
                    options.splash.unmask();
                }
            },
            'appslistload': function (options) {
                options.splash.unmask();
                Ext.Viewport.setActiveItem({xtype:'homepanel'});
            }
        }
    },

    launch: function(app) {
        console.log("Launched");
        this.loadConfigData(Ext.bind(function () {
            console.log(lpad.util.Config.get('env'));
            Ext.Viewport.setActiveItem({xtype:'splashpanel'});
        }, this));

	},
	loadConfigData: function (callback) {
        console.log("Load config");
        lpad.util.Config.getInstance().loadFromFile(lpad.app.configFile, function (res) {
            callback();
            if (res instanceof Error) {
                Ext.Logger.error("Unable to load config file!");
                return;
            }
        }.bind(this));
    },
    loadDeviceData: function (container) {
        //Temporarily stored user id in local storage; need WAC cookie
        var userLogin = localStorage.getItem('userId') || 'test';

        // We can use "set" method to set additional data required by ESB
        Ext.create('lpad.util.DeviceInfo').set('userLogin', userLogin).askServer(function (res) {
            if (res instanceof Error) {
                Ext.Msg.alert('Data Loading Error');
            } else {
                lpad.util.Config.set(res);
                Ext.Logger.info('Additional data loaded');
            }
            this.fireEvent('deviceinfoload', {
                isManaged: this._checkDeviceData(container),
                splash: container
            });
        }.bind(this));
    },
    loadAppsList: function (splash) {
        var me = this, store = Ext.create('lpad.store.Apps');
        store.on('load', function (store, records) {

            lpad.util.EsbRequest.getEsbData({
                url: lpad.util.Config.get('appsUrl'),
                staticUrl: lpad.util.Config.get('staticAppsUrl'),
                method: 'get',
                callback: function (options, success, response) {
                    var data;
                    if (!success) {
                        throw new Error("Applications data could not be loaded!");
                    }
                    data = Ext.JSON.decode(response.responseText).resultSet.results;
                    if (!data || !Ext.isArray(data)) {
                        throw new Error("Invalid applications data!");
                    }
                    store.compareExistingApps(data);
                    store.sync();
                    store.destroy();
                    me.fireEvent('appslistload', {
                        splash: splash
                    });
                },
                scope: this
            });
        });
        store.load();
    },
    _checkDeviceData: function (splashPanel) {
        var conf = lpad.util.Config;
        if (conf.get('requireManagedDevice') === true && conf.get('isManaged') === false) {
            splashPanel.setMessage("This device is not managed!");
            return false;
        }
        if (conf.get('requireInternalAccess') === true && conf.get('isConnectedInternal') === false) {
            splashPanel.setMessage("This device is not connected to VPN!<br /><center><a href='" + conf.get('vpnAppUrl') +"'>connect using Juno Pulse</a></center>");
            return false;
        }
        return true;
    }
});
