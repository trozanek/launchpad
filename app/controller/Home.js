Ext.define('lpad.controller.Home', {
    extend: 'Ext.app.Controller',

    requires: [

    ],

    config: {
        control: {
            'homepanel toolbar button[action=settings]': {
                tap: 'goToSettings'
            }
        }
    },
    goToSettings: function (button) {
        console.log('go to settings');
        console.dir(button.up('homepanel'));
       button.up('homepanel').setActiveItem(1);
    }
});
