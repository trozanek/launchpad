Ext.define('lpad.view.Splash',{
	extend: 'Ext.Panel',
	xtype: 'splashpanel',
    config: {
        layout: 'vbox',
        items: [
            {
                html:'<div id="logo" style="max-width:208px;margin:auto;padding-top:50px">'+
                    '<img src="resources/images/genelogo.png"/>'+
                    '</div>'
            },
            {
                xtype:'panel',
                html:'',
                centered: true,
                hidden: true,
                padding: '5px 10px'
            }
        ]
    },
    setMessage: function (msg) {
        this.getAt(1).setHtml(msg).setHidden(false);
    }
});

