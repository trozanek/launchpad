Ext.define('lpad.view.Springboard', {
    extend: 'lpad.util.springboard.DataView',
    xtype: 'springboard',

    config: {
        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                title: "Launchpad",
                items: [
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'button',
                        iconCls: 'settings1',
                        iconMask: 'true',
                        action: 'settings'
                    }
                ]
            }
        ],
        cls: 'springboard',
        store: {
            xtype: 'appsstore',
            filters: [
                {
                    property: 'active',
                    value   : true
                }
            ],
            autoLoad: true
        }
    }
});
