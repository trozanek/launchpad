Ext.define('lpad.view.Bubble', {
	extend : 'Ext.Panel',
	alias: 'bubble',
	xtype : 'bubble',

	config : {
		title : 'Bubble',
		cls : 'bubble',
		layout: 'hbox',
		showAnimation: 'fadeIn',
		items : [
			{
                xtype: 'image',
                src: lpad.app.icon[57],
                cls : 'app-icon',
                flex: 2
            },
			{
				flex: 3,
				cls : 'instructions',
				html : '<p>Install this app on your phone: tap <img src="resources/images/bubble/ios-fav-icon.png"> or ' +
                    '<img src="resources/images/bubble/ios-fav-icon-plus.png"> and then <strong>&rsquo;Add to Home Screen&rsquo;</strong></p>'
			},
			{
				flex: 1,
				cls: 'btn-wrap',
				items : [
					{
						xtype : 'button',
						text : 'x',
						iconCls: 'close',
						action: 'close',
						cls: 'close-button'
					}
				]
			},
			{
				id : 'bubbleArrow',
				cls : 'arrow-down',
				html : '<div class="border"></div><div class="arrow"></div>'
			}
		],
		centered : 'true'
    },
    initialize : function() {
        var el = this, arrow = Ext.get('bubbleArrow');
		if (Ext.os.is.iPhone) {
            el.addCls('iphone');
        } else if (Ext.os.is.iPad || Ext.os.is['iPad Simulator']) {
            el.addCls('ipad');
            arrow.removeCls('arrow-down');
            arrow.addCls('arrow-up');
        }
    }

});
