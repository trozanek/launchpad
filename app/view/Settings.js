Ext.define("lpad.view.Settings", {
    extend: 'Ext.DataView',
    xtype: 'settingsview',
    requires: [
        'lpad.view.settings.Item'
    ],
    config: {
        useComponents: true,
        defaultType: 'settingsitem',
        store: {
            xtype: 'appsstore'
        },
        items: [
            {
                xtype:'toolbar',
                docked: 'top',
                title: "Settings",
                items: [
                    {
                        text: "Back",
                        ui: 'back',
                        action: 'back'
                    }
                ]
            }
        ]
    }
});
