Ext.define('lpad.view.settings.Item', {
    extend: 'Ext.dataview.component.DataItem',
    requires: [
        'Ext.field.Toggle'
    ],
    xtype: 'settingsitem',

    config: {
        dataMap: {
            getField: {
                setLabel: 'name',
                setName: 'name',
                setValue: 'active'
            }
        },
        field: {
            xtype: 'togglefield',
            name: '',
            label: '-',
            labelWidth: '60%'
        }
    },

    applyField: function (config) {
        return Ext.factory(config, Ext.field.Toggle, this.getField());
    },
    updateField: function(newField, oldField) {
        if (oldField) {
            this.remove(oldField);
        }

        if (newField) {
            this.add(newField);
        }
    }
});
