Ext.define("lpad.view.Home", {
    extend: 'Ext.Panel',
    xtype: 'homepanel',
    config: {
        layout: 'card',

        items: [
            {
                xtype: 'springboard'
            },
            {
                xtype: 'settingsview'
            }
        ]
    }
});
