Ext.define('lpad.model.Apps', {
    extend: 'Ext.data.Model',
    requires: [
        'Ext.data.identifier.Uuid'
    ],
    config: {
        identifier: {
            type: 'uuid'
        },
        fields: [
            'id',
            '_uid',
            'application_name',
            'application_url',
            'application_icon_url',
            'last_updated_by',
            'last_updated_timestamp',
            //just for more consistent interface
            { name: 'name', convert: function (v, record) {
                return record.get('application_name');
            }},
            { name: 'url', convert: function (v, record) {
                return record.get('application_url');
            }},
            { name: 'icon', convert: function (v, record) {
                return record.get('application_icon_url');
            }},
            { name: 'active', type: 'boolean', defaultValue: true }
        ],
        idProperty: '_uid',
        proxy: {
            type: 'localstorage',
            id  : 'lpad-settings'
        }
    }
});
